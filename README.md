# Sudoku
Instant sudoku solver!

Input your sudoku puzzle. Replace blank numbers with "_". You can use spaces and tabs to help you format it.

Ex:
```
8 _ _   _ _ _   _ _ _
_ _ 3   6 _ _   _ _ _
_ 7 _   _ 9 _   2 _ _


_ 5 _   _ _ 7   _ _ _
_ _ _   _ 4 5   7 _ _
_ _ _   1 _ _   _ 3 _


_ _ 1   _ _ _   _ 6 8
_ _ 8   5 _ _   _ 1 _
_ 9 _   _ _ _   4 _ _

```